import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="Pytest Example",
    version="0.0.1",
    author="Khoi Ngo",
    author_email="quainhan100@gmail.com",
    description="Just test pytest",
    long_description = long_description,
    long_description_content_type="text/markdown",
    url="",
    packages=setuptools.find_packages(),
    clasifiers=[
        "Programming Language :: PYTHON :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS independent",
    ],
)